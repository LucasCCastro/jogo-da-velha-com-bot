/*
  Lucas Cordeiro Castro
  14/05/2019
 */

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int ultimaJogada = -1;
        JogoDaVelha game = new JogoDaVelha();
        game.iniciarTabela();
        System.out.println("      ==|JOGO DA VELHA|==");
        System.out.println("   Bem vindo ao jogo da velha!\n     Seu simbolo será o 'X'.");
        do {
            int linha, coluna;

            System.out.println("\n______________________________");
            System.out.println("\n\t   Jogada do bot:\n");
            game.jogadaBot();
            ultimaJogada = 1; //Significa que o BOT jogou por ultimo
            game.printarTabela();

            if (!game.checaVitoria() && !game.taCheia()) {
                do {

                    System.out.print("\n\t      Sua vez!");
                    System.out.print("\nInforme o numero da linha desejada: ");
                    linha = in.nextInt() - 1;
                    System.out.print("Informe o numero da coluna desejada: ");
                    coluna = in.nextInt() - 1;
                    ultimaJogada = 0; //Significa que o usuario jogou por ultimo
                    System.out.print("\n");

                }
                while (!game.fazJogada(linha, coluna));
                System.out.println("\t    Sua jogada:\n");
                game.printarTabela();
            }
        }
        while (!game.checaVitoria() && !game.taCheia());
        if (game.taCheia() && !game.checaVitoria()) {
            System.out.println("     Jogo terminou em empate!");
        } else {
            if (ultimaJogada == 0) System.out.println("\n\t  VOCE VENCEU!!!");
            if (ultimaJogada == 1) System.out.println("\n\t  Voce Perdeu!");
        }
    }
}