# Jogo da velha com BOT

**Este projeto se trata de um jogo da velha bem simples, desenvolvido em java, que criei em meus primeiros contatos com programação.**


### O que o programa faz:

- O app é um jogo da velha feito diretamente no terminal;
- A partida é feita do usuário real, contra jogadas automáticas de um BOT;
- As jogadas do BOT são totalmente baseadas em cima das jogadas do usuário;
- O Bot foi programado para não perder, apenas vencer ou empatar.