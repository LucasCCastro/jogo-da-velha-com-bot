/**
 * Lucas Cordeiro Castro
 * 14/05/2019
 */

public class JogoDaVelha {
    private char[][] tabela;
    private int jornada = -1;

    public JogoDaVelha() {
        tabela = new char[3][3];
        iniciarTabela();
    }

    //Criar a tabela de inicio
    public void iniciarTabela() {
        //Loop pelas linhas
        for (int i = 0; i < 3; i++) {
            // Loop pelas colunas
            for (int j = 0; j < 3; j++) {
                tabela[i][j] = '-';
            }
        }
    }

    //Printar a tabela atual
    public void printarTabela() {
        int cont = 1;
        System.out.println("\t    1   2   3");
        System.out.println("\t  -------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("\t" + cont + " | ");
            for (int j = 0; j < 3; j++) {
                System.out.print(tabela[i][j] + " | ");
            }
            System.out.println();
            System.out.println("\t  -------------");
            cont++;
        }
    }

    //Verificar se a tabela ja esta cheia
    public boolean taCheia() {
        boolean taCheia = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tabela[i][j] == '-') {
                    taCheia = false;
                    break;
                }
            }
        }
        return taCheia;
    }

    //Retorna verdade se alguem venceu, senao retorna falso
    //Esse metodo chama os outros metodos de checagem de vitoria
    public boolean checaVitoria() {
        return (checaLinhas() || checaColunas() || checaDiagonais());
    }

    // Loop entre as linhas para verificar se ha algum vencedor
    private boolean checaLinhas() {
        for (int i = 0; i < 3; i++) {
            if (checaLinhaColuna(tabela[i][0], tabela[i][1], tabela[i][2])) {
                return true;
            }
        }
        return false;
    }

    // Loop entre as colunas para verificar se ha algum vencedor
    private boolean checaColunas() {
        for (int i = 0; i < 3; i++) {
            if (checaLinhaColuna(tabela[0][i], tabela[1][i], tabela[2][i])) {
                return true;
            }
        }
        return false;
    }

    // Checa as duas diagonais para verificar se em alguma delas ha um vencedor
    private boolean checaDiagonais() {
        return ((checaLinhaColuna(tabela[0][0], tabela[1][1], tabela[2][2])) || (checaLinhaColuna(tabela[0][2], tabela[1][1], tabela[2][0])));
    }

    // Checa se todos os 3 valores tem o mesmo simbolo (X ou O), indicando vitoria.
    private boolean checaLinhaColuna(char c1, char c2, char c3) {
        return ((c1 != '-') && (c1 == c2) && (c2 == c3));
    }

    // Realiza a jogada posicionando o X no lugar indicado pelo jogador
    public boolean fazJogada(int linha, int col) {
        // Checa se a linha e coluna solicitada pelo usuario estao dentro dos limites possiveis (3x3)
        if ((linha >= 0) && (linha < 3)) {
            if ((col >= 0) && (col < 3)) {
                if (tabela[linha][col] == '-') {
                    tabela[linha][col] = 'X';
                    return true;
                }
            }
        }
        return false;
    }

    // Realiza a jogada do BOT 
    public void jogadaBot() {
        int cont = 0; //Vai entrar no loop abaixo para fazer a contagem de que rodada o jogo se encontra e orientar as acoes do bot
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tabela[i][j] == '-') cont++;
            }
        }
        //Bot inicia o jogo colocando no canto inferior direito.
        if (cont == 9) {
            tabela[2][2] = 'O';
            return;
        }

        //Se for a 3 rodada e o usuario colocou em algum LADO, entao o bot coloca no meio.
        if (cont == 7 && (tabela[1][2] == 'X' || tabela[0][1] == 'X' || tabela[1][0] == 'X' || tabela[2][1] == 'X')) {
            tabela[1][1] = 'O';
            jornada = 1;
            return;
        }
        //Se for a 5 rodada e o usuario nao bloqueou a posicao [1][1], entao o bot coloca ali e vence.
        if (jornada == 1 && cont == 5 && tabela[0][0] == '-') {
            tabela[0][0] = 'O';
            return;
        }
        //Se for a 5 rodada e o usuario bloqueou a posicao [1][1], entao dependendo do lado que ele escolheu na 2 rodada, o bot escolhe o lado correto para prosseguir.
        if (jornada == 1 && cont == 5 && tabela[0][0] == 'X' && (tabela[1][2] == 'X' || tabela[1][0] == 'X')) {
            tabela[2][0] = 'O';
            return;
        }
        if (jornada == 1 && cont == 5 && tabela[0][0] == 'X' && (tabela[0][1] == 'X' || tabela[2][1] == 'X')) {
            tabela[0][2] = 'O';
            return;
        }
        //Se for a 7 rodada, entao dependendo de onde o bot colocou na 5 rodada, so falta finalizar no local certo para vencer.
        if (jornada == 1 && cont == 3 && tabela[2][0] == 'O' && tabela[2][1] == 'X') {
            tabela[0][2] = 'O';
            return;
        }
        if (jornada == 1 && cont == 3 && tabela[2][0] == 'O' && tabela[0][2] == 'X') {
            tabela[2][1] = 'O';
            return;
        }
        if (jornada == 1 && cont == 3 && tabela[0][2] == 'O' && tabela[1][2] == 'X') {
            tabela[2][0] = 'O';
            return;
        }
        if (jornada == 1 && cont == 3 && tabela[0][2] == 'O' && tabela[2][0] == 'X') {
            tabela[1][2] = 'O';
            return;
        }
        //Se o usuario nao bloqueou nenhum espaco livre que da a vitoria pro bot, entao ele coloca em um deles e vence.
        if (jornada == 1 && cont == 3 && tabela[2][0] == 'O' && tabela[2][1] == '-') {
            tabela[2][1] = 'O';
            return;
        }
        if (jornada == 1 && cont == 3 && tabela[2][0] == 'O' && tabela[0][2] == '-') {
            tabela[0][2] = 'O';
            return;
        }
        if (jornada == 1 && cont == 3 && tabela[0][2] == 'O' && tabela[1][2] == '-') {
            tabela[1][2] = 'O';
            return;
        }
        if (jornada == 1 && cont == 3 && tabela[0][2] == 'O' && tabela[2][0] == '-') {
            tabela[2][0] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Se for a 3 rodada e o usuario colocou no CANTO inferior esquerdo, entao o bot coloca no canto superior direito.
        if (cont == 7 && tabela[2][0] == 'X') {
            tabela[0][2] = 'O';
            jornada = 2;
            return;
        }
        //Se for a 5 rodada e o usuario nao bloqueou a posicao [2][3], entao o bot coloca ali e vence.
        if (jornada == 2 && cont == 5 && tabela[1][2] == '-') {
            tabela[1][2] = 'O';
            return;
        }
        //Se for a 5 rodada e o usuario bloqueou a posicao [2][3], entao o bot coloca no canto superior esquerdo.
        if (jornada == 2 && cont == 5 && tabela[1][2] == 'X') {
            tabela[0][0] = 'O';
            return;
        }
        //Se for a 7 rodada, entao dependendo de onde o usuario jogou para tentar bloquear a vitoria, o bot finaliza colocando no outro espaco.
        if (jornada == 2 && cont == 3 && tabela[1][1] == 'X') {
            tabela[0][1] = 'O';
            return;
        }
        if (jornada == 2 && cont == 3 && tabela[0][1] == 'X') {
            tabela[1][1] = 'O';
            return;
        }
        //Se o usuario nao bloqueou algum dos espacos que garante a vitoria ao bot, entao o bot coloca em um deles e vence.
        if (jornada == 2 && cont == 3 && tabela[1][1] == '-') {
            tabela[1][1] = 'O';
            return;
        }
        if (jornada == 2 && cont == 3 && tabela[0][1] == '-') {
            tabela[0][1] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Se for a 3 rodada e o usuario colocou no CANTO superior direito, entao o bot coloca no canto inferior esquerdo.
        if (cont == 7 && tabela[0][2] == 'X') {
            tabela[2][0] = 'O';
            jornada = 3;
            return;
        }
        //Se for a 5 rodada e o usuario nao bloqueou a posicao [3][2], entao o bot coloca ali e vence.
        if (jornada == 3 && cont == 5 && tabela[2][1] == '-') {
            tabela[2][1] = 'O';
            return;
        }
        //Se for a 5 rodada e o usuario bloqueou a posicao [3][2], entao o bot coloca no canto superior esquerdo.
        if (jornada == 3 && cont == 5 && tabela[2][1] == 'X') {
            tabela[0][0] = 'O';
            return;
        }
        //Se for a 7 rodada, entao dependendo de onde o usuario jogou na ultima rodada para tentar bloquear a vitoria, o bot coloca no outro espaco e vence.
        if (jornada == 3 && cont == 3 && tabela[1][0] == 'X') {
            tabela[1][1] = 'O';
            return;
        }
        if (jornada == 3 && cont == 3 && tabela[1][1] == 'X') {
            tabela[1][0] = 'O';
            return;
        }
        //Se o usuario nao bloqueou nenhuma das duas possibilidades do bot de vencer, o bot so coloca em uma delas e vence.
        if (jornada == 3 && cont == 3 && tabela[1][0] == '-') {
            tabela[1][0] = 'O';
            return;
        }
        if (jornada == 3 && cont == 3 && tabela[1][1] == '-') {
            tabela[1][1] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Se for a 3 rodada e o usuario colocou no CANTO superior esquerdo, entao o bot joga no canto inferior esquerdo.
        if (cont == 7 && tabela[0][0] == 'X') {
            tabela[2][0] = 'O';
            jornada = 4;
            return;
        }
        //Se for a 5 rodada e o usuario nao bloqueou a posicao [3][2], entao o bot coloca ali e vence.
        if (jornada == 4 && cont == 5 && tabela[2][1] == '-') {
            tabela[2][1] = 'O';
            return;
        }
        //Se for a 5 rodada e o usuario bloqueou a posicao [3][2], entao o bot coloca no canto superior direito.
        if (jornada == 4 && cont == 5 && tabela[2][1] == 'X') {
            tabela[0][2] = 'O';
            return;
        }
        //Se for a 7 rodada, entao dependendo de onde o usuario jogou na ultima rodada, o bot finaliza no outro espaco disponivel e vence.
        if (jornada == 4 && cont == 3 && tabela[1][1] == 'X') {
            tabela[1][2] = 'O';
            return;
        }
        if (jornada == 4 && cont == 3 && tabela[1][2] == 'X') {
            tabela[1][1] = 'O';
            return;
        }
        //Se o usuario nao bloqueou nenhuma finalizacao, entao o bot so coloco em uma dessas posicoes e vence.
        if (jornada == 4 && cont == 3 && tabela[1][1] == '-') {
            tabela[1][1] = 'O';
            return;
        }
        if (jornada == 4 && cont == 3 && tabela[1][2] == '-') {
            tabela[1][2] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Se for a 3 rodada e o usuario colocou no MEIO, entao o bot coloca no canto superior esquerdo
        if (cont == 7 && tabela[1][1] == 'X') {
            tabela[0][0] = 'O';
            jornada = 5;
            return;
        }
        //Se for a 5 rodada e o usuario colocou em algum LADO, o bot tem que bloquear toda tentativa de vitoria do oponente.

        //LADO CIMA
        if (jornada == 5 && cont == 5 && tabela[0][1] == 'X') {
            tabela[2][1] = 'O';
            jornada = 6;
            return;
        }
        //Se o usuario nao bloquear no [3][1] o bot coloca ali e vence.
        if (jornada == 6 && cont == 3 && tabela[2][0] == '-') {
            tabela[2][0] = 'O';
            return;
        }
        //Se bloquear, entao o bot coloca no canto superior direito.
        if (jornada == 6 && cont == 3 && tabela[2][0] == 'X') {
            tabela[0][2] = 'O';
            return;
        }
        //Se nao bloquear na posicao [2][3] o bot coloca ali e vence.
        if (jornada == 6 && cont == 1 && tabela[1][2] == '-') {
            tabela[1][2] = 'O';
            return;
        }
        //Se bloquear, entao o bot coloca no outro local, e da empate.
        if (jornada == 6 && cont == 1 && tabela[1][2] == 'X') {
            tabela[1][0] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //LADO ESQUERDA
        if (jornada == 5 && cont == 5 && tabela[1][0] == 'X') {
            tabela[1][2] = 'O';
            jornada = 7;
            return;
        }
        //Se o usuario nao bloquear na posicao [1][3] o bot coloca ali e vence.
        if (jornada == 7 && cont == 3 && tabela[0][2] == '-') {
            tabela[0][2] = 'O';
            return;
        }
        //Se bloquear, entao o bot coloca no canto superior direito.
        if (jornada == 7 && cont == 3 && tabela[0][2] == 'X') {
            tabela[2][0] = 'O';
            return;
        }
        //Se o usuario nao bloquear na posicao [3][2] o bot coloca ali e vence.
        if (jornada == 7 && cont == 1 && tabela[2][1] == '-') {
            tabela[2][1] = 'O';
            return;
        }
        //Se bloquear, entao o bot coloca no outro local, e da empate.
        if (jornada == 7 && cont == 1 && tabela[2][1] == 'X') {
            tabela[0][1] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //LADO BAIXO
        if (jornada == 5 && cont == 5 && tabela[2][1] == 'X') {
            tabela[0][1] = 'O';
            jornada = 8;
            return;
        }
        //Se o usuario nao bloquear na posicao [1][3] o bot coloca ali e vence.
        if (jornada == 8 && cont == 3 && tabela[0][2] == '-') {
            tabela[0][2] = 'O';
            return;
        }
        //Se bloquear, entao o bot coloca no canto inferior esquerdo.
        if (jornada == 8 && cont == 3 && tabela[0][2] == 'X') {
            tabela[2][0] = 'O';
            return;
        }
        //Se o usuario nao bloquear na posicao [2][1], entao o bot coloca ali e vence.
        if (jornada == 8 && cont == 1 && tabela[1][0] == '-') {
            tabela[1][0] = 'O';
            return;
        }
        //Se bloquear, entao o bot coloca no outro local, e da empate.
        if (jornada == 8 && cont == 1 && tabela[1][0] == 'X') {
            tabela[1][2] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //LADO DIREITA
        if (jornada == 5 && cont == 5 && tabela[1][2] == 'X') {
            tabela[1][0] = 'O';
            jornada = 9;
            return;
        }
        //Se o usuario nao bloquear na posicao [3][1], o bot coloca ali e vence.
        if (jornada == 9 && cont == 3 && tabela[2][0] == '-') {
            tabela[2][0] = 'O';
            return;
        }
        //Se bloquear, entao o bot coloca no canto superior direito.
        if (jornada == 9 && cont == 3 && tabela[2][0] == 'X') {
            tabela[0][2] = 'O';
            return;
        }
        //Se o usuario nao bloquear na posicao [1][2], entao o bot coloca ali e vence.
        if (jornada == 9 && cont == 1 && tabela[0][1] == '-') {
            tabela[0][1] = 'O';
            return;
        }
        //Se bloquear, entao o bot coloca no outro local, e da empate.
        if (jornada == 9 && cont == 1 && tabela[0][1] == 'X') {
            tabela[2][1] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Se for a 5 rodada e o usuario jogou em algum CANTO

        //CANTO SUPERIOR DIREITO
        if (jornada == 5 && cont == 5 && tabela[0][2] == 'X') {
            tabela[2][0] = 'O';
            jornada = 10;
            return;
        }
        if (jornada == 10 && cont == 3 && tabela[1][0] == 'X') {
            tabela[2][1] = 'O';
            return;
        }
        if (jornada == 10 && cont == 3 && tabela[2][1] == 'X') {
            tabela[1][0] = 'O';
            return;
        }
        //Se o usuario nao bloqueou algum dos espacos que dao a vitoria ao bot, o bot so coloca em um desses espacos e vence.
        if (jornada == 10 && cont == 3 && tabela[1][0] == '-') {
            tabela[1][0] = 'O';
            return;
        }
        if (jornada == 10 && cont == 3 && tabela[2][1] == '-') {
            tabela[2][1] = 'O';
            return;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //CANTO INFERIOR ESQUERDO
        if (jornada == 5 && cont == 5 && tabela[2][0] == 'X') {
            tabela[0][2] = 'O';
            jornada = 11;
            return;
        }
        if (jornada == 11 && cont == 3 && tabela[0][1] == 'X') {
            tabela[1][2] = 'O';
            return;
        }
        if (jornada == 11 && cont == 3 && tabela[1][2] == 'X') {
            tabela[0][1] = 'O';
            return;
        }
        //Se o usuario nao bloqueou algum dos espacos que dao a vitoria ao bot, o bot so coloca em um desses espacos e vence.
        if (jornada == 11 && cont == 3 && tabela[0][1] == '-') {
            tabela[0][1] = 'O';
            return;
        }
        if (jornada == 11 && cont == 3 && tabela[1][2] == '-') {
            tabela[1][2] = 'O';
        }


    }
}
